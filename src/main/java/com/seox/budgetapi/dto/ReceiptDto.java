package com.seox.budgetapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * A DTO for the {@link com.seox.budgetapi.model.Receipt} entity
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public record ReceiptDto(String storeName, LocalDate purchaseDate, @JsonProperty(access = JsonProperty.Access.READ_ONLY) List<ProductDto> products) implements Serializable {
}