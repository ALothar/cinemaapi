package com.seox.budgetapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * A DTO for the {@link com.seox.budgetapi.model.Receipt} entity
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public record WritableReceiptDto(String storeName, LocalDate purchaseDate, List<ProductDto> products) implements Serializable {
}