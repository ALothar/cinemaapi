package com.seox.budgetapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.seox.budgetapi.model.enums.Measurement;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A DTO for the {@link com.seox.budgetapi.model.Product} entity
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public record ProductDto(String name, BigDecimal price, Measurement measurement, BigDecimal quantity, String description) implements Serializable {
}