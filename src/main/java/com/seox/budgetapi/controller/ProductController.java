package com.seox.budgetapi.controller;

import com.seox.budgetapi.dto.ProductDto;
import com.seox.budgetapi.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/v{apiVersion}/product")
@AllArgsConstructor
public class ProductController {

    private final ProductService service;

    @GetMapping("/")
    public List<ProductDto> getAllProducts(@PathVariable final String apiVersion) {
        return switch (apiVersion) {
            case "1" -> getAllProductsV1();
            default -> Collections.emptyList();
        };
    }

    private List<ProductDto> getAllProductsV1(){
        return service.findAll();
    }

    @GetMapping("/receipt")
    public List<ProductDto> getProductsByReceiptId(@PathVariable String apiVersion,
                                                   @RequestParam(name = "id") Long id) {
        return switch (apiVersion) {
            case "1" -> getProductsByReceiptIdV1(id);
            default -> Collections.emptyList();
        };
    }

    private List<ProductDto> getProductsByReceiptIdV1(final Long id) {
        return service.findByReceiptId(id);
    }

    @GetMapping
    public List<ProductDto> getProductsByName(@PathVariable String apiVersion,
                                              @RequestParam(name = "productName") String productName) {
        return switch (apiVersion) {
            case "1" -> getProductsByNameIdV1(productName);
            default -> Collections.emptyList();
        };
    }

    private List<ProductDto> getProductsByNameIdV1(final String productName) {
        return service.findByName(productName);
    }
}
