package com.seox.budgetapi.controller;

import com.seox.budgetapi.dto.ReceiptDto;
import com.seox.budgetapi.dto.WritableReceiptDto;
import com.seox.budgetapi.service.ReceiptService;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v{apiVersion}/receipt")
@AllArgsConstructor
public class ReceiptController {

    private final ReceiptService service;

    @GetMapping("/")
    public List<ReceiptDto> getAllProducts(@PathVariable final String apiVersion) {
        return switch (apiVersion) {
            case "1" -> getAllProductsV1();
            default -> Collections.emptyList();
        };
    }

    private List<ReceiptDto> getAllProductsV1(){
        return service.findAll();
    }

    @GetMapping("/store")
    public List<ReceiptDto> getProductsByStoreName(@PathVariable final String apiVersion,
                                                   @RequestParam(name = "storeName") final String storeName) {
        return switch (apiVersion) {
            case "1" -> getProductsByStoreNameV1(storeName);
            default -> Collections.emptyList();
        };
    }

    private List<ReceiptDto> getProductsByStoreNameV1(final String storeName) {
        return service.findByStoreName(storeName);
    }

    @GetMapping
    public List<ReceiptDto> getProductsByPurchaseDate(@PathVariable final String apiVersion,
                                                      @RequestParam(name = "purchaseDate")
                                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate purchaseDate) {
        return switch (apiVersion) {
            case "1" -> getProductsByPurchaseDateV1(purchaseDate);
            default -> Collections.emptyList();
        };
    }

    private List<ReceiptDto> getProductsByPurchaseDateV1(final LocalDate purchaseDate) {
        return service.findByPurchaseDate(purchaseDate);
    }

    @GetMapping("/between")
    public List<ReceiptDto> getProductsByPurchaseDateBetween(@PathVariable final String apiVersion,
                                                             @RequestParam(name = "purchaseDateStart", required = false)
                                                             @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate purchaseDateStart,
                                                             @RequestParam(name = "purchaseDateEnd")
                                                             @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate purchaseDateEnd) {
        return switch (apiVersion) {
            case "1" -> purchaseDateStart == null ? getProductsByPurchaseDateBetweenV1(LocalDate.now(), purchaseDateEnd) : getProductsByPurchaseDateBetweenV1(purchaseDateStart, purchaseDateEnd);
            default -> Collections.emptyList();
        };
    }

    private List<ReceiptDto> getProductsByPurchaseDateBetweenV1(final LocalDate purchaseDateStart, final LocalDate purchaseDateEnd) {
        return service.findByPurchaseDateBetween(purchaseDateStart, purchaseDateEnd);
    }

    @GetMapping("/after")
    public List<ReceiptDto> getProductsByPurchaseDateAfter(@PathVariable final String apiVersion,
                                                             @RequestParam(name = "purchaseDateAfter")
                                                             @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate purchaseDateAfter) {
        return switch (apiVersion) {
            case "1" -> getProductsByPurchaseDateAfterV1(purchaseDateAfter);
            default -> Collections.emptyList();
        };
    }

    private List<ReceiptDto> getProductsByPurchaseDateAfterV1(final LocalDate purchaseDateAfter) {
        return service.findByPurchaseDateAfter(purchaseDateAfter);
    }

    @GetMapping("/before")
    public List<ReceiptDto> getProductsByPurchaseDateBefore(@PathVariable final String apiVersion,
                                                             @RequestParam(name = "purchaseDateBefore")
                                                             @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate purchaseDateBefore) {
        return switch (apiVersion) {
            case "1" -> getProductsByPurchaseDateBeforeV1(purchaseDateBefore);
            default -> Collections.emptyList();
        };
    }

    private List<ReceiptDto> getProductsByPurchaseDateBeforeV1(final LocalDate purchaseDateBefore) {
        return service.findByPurchaseDateBefore(purchaseDateBefore);
    }

    @PostMapping("/add")
    public Map<String,String> addReceipt(@PathVariable final String apiVersion,
                          @RequestBody final List<WritableReceiptDto> receiptDto) {
        return switch (apiVersion) {
            case "1" -> addReceiptV1(receiptDto);
            default -> Collections.singletonMap("status", "Please select viable API version");
        };
    }

    private Map<String,String> addReceiptV1(final List<WritableReceiptDto> receiptDto) {
        return Collections.singletonMap("status", service.addReceipt(receiptDto).isEmpty() ? "Error" : "Success");
    }
}
