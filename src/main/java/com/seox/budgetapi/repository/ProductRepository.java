package com.seox.budgetapi.repository;

import com.seox.budgetapi.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findByNameLike(final String name);
    List<Product> findByReceiptId(final Long receiptId);
}