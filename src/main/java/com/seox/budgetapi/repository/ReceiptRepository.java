package com.seox.budgetapi.repository;

import com.seox.budgetapi.model.Receipt;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ReceiptRepository extends JpaRepository<Receipt, Long> {
    List<Receipt> findByStoreNameLike(final String name);

    List<Receipt> findByPurchaseDate(final LocalDate purchaseDate);

    List<Receipt> findByPurchaseDateBetween(final LocalDate purchaseDateStart, final LocalDate purchaseDateEnd);

    List<Receipt> findByPurchaseDateAfter(final LocalDate purchaseDate);

    List<Receipt> findByPurchaseDateBefore(final LocalDate purchaseDate);
}