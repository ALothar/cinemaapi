package com.seox.budgetapi.config;

import com.seox.budgetapi.dto.ProductDto;
import com.seox.budgetapi.dto.ReceiptDto;
import com.seox.budgetapi.dto.WritableReceiptDto;
import com.seox.budgetapi.model.Product;
import com.seox.budgetapi.model.Receipt;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProjectMapper {

    ProductDto entityToDto(Product product);

    ReceiptDto entityToDto(Receipt receipt);

    Product dtoToEntity(ProductDto dto);

    Receipt dtoToEntity(ReceiptDto dto);

    Receipt writableDtoToEntity(WritableReceiptDto dto);
}
