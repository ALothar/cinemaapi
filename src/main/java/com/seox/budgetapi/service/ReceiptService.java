package com.seox.budgetapi.service;

import com.seox.budgetapi.config.ProjectMapper;
import com.seox.budgetapi.dto.ReceiptDto;
import com.seox.budgetapi.dto.WritableReceiptDto;
import com.seox.budgetapi.model.Receipt;
import com.seox.budgetapi.repository.ReceiptRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@AllArgsConstructor
public class ReceiptService {

    private final ReceiptRepository repository;
    private final ProjectMapper mapper;

    public List<ReceiptDto> findAll() {
        log.debug("Request findAll <-");
        final List<ReceiptDto> result = repository.findAll().stream().map(mapper::entityToDto).toList();
        log.debug("Request findAll -> " + result);
        return result;
    }

    public List<ReceiptDto> findByStoreName(final String name) {
        log.debug("Request findByStoreName <- name={}", name);
        final List<ReceiptDto> result = repository.findByStoreNameLike(name).stream().map(mapper::entityToDto).toList();
        log.debug("Request findByStoreName -> name={} : {}", name, result);
        return result;
    }

    public List<ReceiptDto> findByPurchaseDate(final LocalDate purchaseDate) {
        log.debug("Request findByPurchaseDate <- date={}", purchaseDate);
        final List<ReceiptDto> result = repository.findByPurchaseDate(purchaseDate).stream().map(mapper::entityToDto).toList();
        log.debug("Request findByPurchaseDate -> date={} : {}", purchaseDate, result);
        return result;
    }

    public List<ReceiptDto> findByPurchaseDateBetween(final LocalDate purchaseDateStart, final LocalDate purchaseDateEnd) {
        log.debug("Request findByPurchaseDateBetween <- dateStart={}, dateEnd={}", purchaseDateStart, purchaseDateEnd);
        final List<ReceiptDto> result = repository.findByPurchaseDateBetween(purchaseDateStart, purchaseDateEnd).stream().map(mapper::entityToDto).toList();
        log.debug("Request findByPurchaseDateBetween -> dateStart={}, dateEnd={}: {}", purchaseDateStart, purchaseDateEnd, result);
        return result;
    }

    public List<ReceiptDto> findByPurchaseDateAfter(final LocalDate purchaseDate) {
        log.debug("Request findByPurchaseDateAfter <- date={}", purchaseDate);
        final List<ReceiptDto> result = repository.findByPurchaseDateAfter(purchaseDate).stream().map(mapper::entityToDto).toList();
        log.debug("Request findByPurchaseDateAfter -> date={} : {}", purchaseDate, result);
        return result;
    }

    public List<ReceiptDto> findByPurchaseDateBefore(final LocalDate purchaseDate) {
        log.debug("Request findByPurchaseDateBefore <- date={}", purchaseDate);
        final List<ReceiptDto> result = repository.findByPurchaseDateBefore(purchaseDate).stream().map(mapper::entityToDto).toList();
        log.debug("Request findByPurchaseDateBefore -> date={} : {}", purchaseDate, result);
        return result;
    }

    public List<ReceiptDto> addReceipt(final List<WritableReceiptDto> receiptList) {
        log.debug("Request addReceipt <- receipt={}", receiptList);
        final List<Receipt> receiptEntityList = receiptList.stream().filter(Objects::nonNull).map(receipt -> {
            final Receipt receiptEntity = mapper.writableDtoToEntity(receipt);
            receiptEntity.getProducts().forEach(product -> product.setReceipt(receiptEntity));
            return repository.save(receiptEntity);
        }).toList();
        final List<ReceiptDto> result = receiptEntityList.stream().map(mapper::entityToDto).toList();
        log.debug("Request addReceipt -> receipt={} : {}", receiptList, result);
        return result;
    }
}
