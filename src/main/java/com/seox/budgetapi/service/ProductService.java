package com.seox.budgetapi.service;

import com.seox.budgetapi.config.ProjectMapper;
import com.seox.budgetapi.dto.ProductDto;
import com.seox.budgetapi.repository.ProductRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class ProductService {

    private final ProductRepository repository;
    private final ProjectMapper mapper;

    public List<ProductDto> findAll() {
        log.debug("Request findAll <-");
        final List<ProductDto> result = repository.findAll().stream().map(mapper::entityToDto).toList();
        log.debug("Request findAll -> " + result);
        return result;
    }

    public List<ProductDto> findByReceiptId(final Long receiptId) {
        log.debug("Request findByReceiptId <- receiptId={}", receiptId);
        final List<ProductDto> result = repository.findByReceiptId(receiptId).stream().map(mapper::entityToDto).toList();
        log.debug("Request findByReceiptId -> receiptId={} : {}", receiptId, result);
        return result;
    }

    public List<ProductDto> findByName(final String name) {
        log.debug("Request findByName <- name={}", name);
        final List<ProductDto> result = repository.findByNameLike(name).stream().map(mapper::entityToDto).toList();
        log.debug("Request findByName -> name={} : {}", name, result);
        return result;
    }
}
