package com.seox.budgetapi.model.enums;

public enum Measurement {
    WEIGHT,QUANTITY,VOLUME
}
