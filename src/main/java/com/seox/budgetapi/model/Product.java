package com.seox.budgetapi.model;

import com.seox.budgetapi.model.enums.Measurement;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;

@Entity(name = "Product")
@Table(name = "product")
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private BigDecimal price;
    @Enumerated(EnumType.STRING)
    private Measurement measurement;
    private BigDecimal quantity;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "receipt_id")
    @ToString.Exclude
    private Receipt receipt;
    private String description;
}
