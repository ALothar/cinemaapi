package com.seox.budgetapi.service;

import com.seox.budgetapi.config.ProjectMapper;
import com.seox.budgetapi.config.ProjectMapperImpl;
import com.seox.budgetapi.dto.ProductDto;
import com.seox.budgetapi.model.Product;
import com.seox.budgetapi.model.Receipt;
import com.seox.budgetapi.model.enums.Measurement;
import com.seox.budgetapi.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = ProjectMapperImpl.class)
@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Autowired
    ProjectMapper mapper;
    @Mock
    ProductRepository repository;
    ProductService service;
    @BeforeEach
    void setUp() {
        service = new ProductService(repository, mapper);
    }

    @Test
    void findAll_twoProductsExist_shouldReturnListOfProductDto() {
        final Product product = Product.builder()
                .id(1L)
                .name("test")
                .price(BigDecimal.valueOf(100))
                .measurement(Measurement.QUANTITY)
                .quantity(BigDecimal.ONE)
                .description("test description")
                .build();
        final Product product2 = Product.builder()
                .id(2L)
                .name("test2")
                .price(BigDecimal.TEN)
                .measurement(Measurement.QUANTITY)
                .quantity(BigDecimal.ONE)
                .description("test description2")
                .build();
        final List<Product> testProducts = Arrays.asList(product, product2);

        when(repository.findAll()).thenReturn(testProducts);

        final List<ProductDto> foundProducts = service.findAll();

        assertNotNull(foundProducts);
        assertFalse(foundProducts.isEmpty());
        assertEquals(testProducts.size(), foundProducts.size());
        assertEquals(testProducts.get(0).getName(), foundProducts.get(0).name());

        verify(repository).findAll();
    }

    @Test
    void findAll_noProductsExist_shouldReturnEmptyList() {
        when(repository.findAll()).thenReturn(Collections.emptyList());
        final List<ProductDto> foundProducts = service.findAll();

        assertNotNull(foundProducts);
        assertTrue(foundProducts.isEmpty());

        verify(repository).findAll();
    }

    @Test
    void findByReceiptId_twoProductsExist_shouldReturnListOfProducts() {
        final Receipt receipt = Receipt.builder()
                .id(1L)
                .build();
        final Product product = Product.builder()
                .id(1L)
                .name("test")
                .price(BigDecimal.valueOf(100))
                .measurement(Measurement.QUANTITY)
                .quantity(BigDecimal.ONE)
                .receipt(receipt)
                .description("test description")
                .build();
        final Product product2 = Product.builder()
                .id(2L)
                .name("test2")
                .price(BigDecimal.TEN)
                .measurement(Measurement.QUANTITY)
                .quantity(BigDecimal.ONE)
                .receipt(receipt)
                .description("test description2")
                .build();
        final List<Product> testProducts = Arrays.asList(product, product2);

        when(repository.findByReceiptId(receipt.getId())).thenReturn(testProducts);

        final List<ProductDto> foundProducts = service.findByReceiptId(receipt.getId());

        assertNotNull(foundProducts);
        assertFalse(foundProducts.isEmpty());
        assertEquals(testProducts.size(), foundProducts.size());
        assertEquals(testProducts.get(0).getName(), foundProducts.get(0).name());

        verify(repository).findByReceiptId(receipt.getId());
    }

    @Test
    void findByReceiptId_noProductsExist_shouldReturnEmptyList() {
        when(repository.findByReceiptId(1L)).thenReturn(Collections.emptyList());
        List<ProductDto> foundProducts = service.findByReceiptId(1L);

        assertNotNull(foundProducts);
        assertTrue(foundProducts.isEmpty());

        verify(repository).findByReceiptId(1L);
    }

    @Test
    void findByName_twoProductsExist_shouldReturnListOfProducts() {
        final Product product = Product.builder()
                .id(1L)
                .name("test")
                .price(BigDecimal.valueOf(100))
                .measurement(Measurement.QUANTITY)
                .quantity(BigDecimal.ONE)
                .description("test description")
                .build();
        final Product product2 = Product.builder()
                .id(2L)
                .name("Test")
                .price(BigDecimal.TEN)
                .measurement(Measurement.QUANTITY)
                .quantity(BigDecimal.ONE)
                .description("test description2")
                .build();
        final List<Product> testProducts = Arrays.asList(product, product2);

        when(repository.findByNameLike("test")).thenReturn(testProducts);

        final List<ProductDto> foundProducts = service.findByName("test");

        assertNotNull(foundProducts);
        assertFalse(foundProducts.isEmpty());
        assertEquals(testProducts.size(), foundProducts.size());
        assertEquals(testProducts.get(0).getName(), foundProducts.get(0).name());

        verify(repository).findByNameLike("test");
    }

    @Test
    void findByName_noProductsExist_shouldReturnEmptyList() {
        when(repository.findByNameLike("test")).thenReturn(Collections.emptyList());
        List<ProductDto> foundProducts = service.findByName("test");

        assertNotNull(foundProducts);
        assertTrue(foundProducts.isEmpty());

        verify(repository).findByNameLike("test");
    }
}