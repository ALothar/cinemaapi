package com.seox.budgetapi.service;

import com.seox.budgetapi.config.ProjectMapper;
import com.seox.budgetapi.config.ProjectMapperImpl;
import com.seox.budgetapi.dto.ProductDto;
import com.seox.budgetapi.dto.ReceiptDto;
import com.seox.budgetapi.dto.WritableReceiptDto;
import com.seox.budgetapi.model.Receipt;
import com.seox.budgetapi.model.enums.Measurement;
import com.seox.budgetapi.repository.ReceiptRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hibernate.internal.util.collections.CollectionHelper.listOf;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = ProjectMapperImpl.class)
@ExtendWith(MockitoExtension.class)
class ReceiptServiceTest {

    @Autowired
    ProjectMapper mapper;
    @Mock
    ReceiptRepository repository;
    ReceiptService service;

    @BeforeEach
    void setUp() {
        service = new ReceiptService(repository, mapper);
    }

    @Test
    void findAll_twoReceiptsExist_shouldReturnListOfReceiptDto() {
        final Receipt receipt = Receipt.builder()
                .id(1L)
                .storeName("test")
                .purchaseDate(LocalDate.now())
                .build();
        final Receipt receipt2 = Receipt.builder()
                .id(2L)
                .storeName("test2")
                .purchaseDate(LocalDate.now())
                .build();
        final List<Receipt> testReceipts = Arrays.asList(receipt, receipt2);

        when(repository.findAll()).thenReturn(testReceipts);

        final List<ReceiptDto> foundReceipts = service.findAll();

        assertNotNull(foundReceipts);
        assertFalse(foundReceipts.isEmpty());
        assertEquals(testReceipts.size(), foundReceipts.size());
        assertEquals(testReceipts.get(0).getStoreName(), foundReceipts.get(0).storeName());

        verify(repository).findAll();
    }

    @Test
    void findAll_noReceiptExist_shouldReturnEmptyList() {
        when(repository.findAll()).thenReturn(Collections.emptyList());
        final List<ReceiptDto> foundReceipts = service.findAll();

        assertNotNull(foundReceipts);
        assertTrue(foundReceipts.isEmpty());

        verify(repository).findAll();
    }

    @Test
    void findByStoreName_twoReceiptsExist_shouldReturnListOfReceiptDto() {
        final Receipt receipt = Receipt.builder()
                .id(1L)
                .storeName("test")
                .purchaseDate(LocalDate.now())
                .build();
        final Receipt receipt2 = Receipt.builder()
                .id(2L)
                .storeName("test")
                .purchaseDate(LocalDate.now())
                .build();
        final List<Receipt> testReceipts = Arrays.asList(receipt, receipt2);

        when(repository.findByStoreNameLike("test")).thenReturn(testReceipts);

        final List<ReceiptDto> foundReceipts = service.findByStoreName("test");

        assertNotNull(foundReceipts);
        assertFalse(foundReceipts.isEmpty());
        assertEquals(testReceipts.size(), foundReceipts.size());
        assertEquals(testReceipts.get(0).getStoreName(), foundReceipts.get(0).storeName());

        verify(repository).findByStoreNameLike("test");
    }

    @Test
    void findByStoreName_noReceiptExist_shouldReturnEmptyList() {
        when(repository.findByStoreNameLike("test")).thenReturn(Collections.emptyList());
        final List<ReceiptDto> foundReceipts = service.findByStoreName("test");

        assertNotNull(foundReceipts);
        assertTrue(foundReceipts.isEmpty());

        verify(repository).findByStoreNameLike("test");
    }

    @Test
    void findByPurchaseDate_twoReceiptsExist_shouldReturnListOfReceiptDto() {
        final LocalDate date = LocalDate.now();
        final Receipt receipt = Receipt.builder()
                .id(1L)
                .storeName("test")
                .purchaseDate(date)
                .build();
        final Receipt receipt2 = Receipt.builder()
                .id(2L)
                .storeName("test")
                .purchaseDate(date)
                .build();
        final List<Receipt> testReceipts = Arrays.asList(receipt, receipt2);

        when(repository.findByPurchaseDate(date)).thenReturn(testReceipts);

        final List<ReceiptDto> foundReceipts = service.findByPurchaseDate(date);

        assertNotNull(foundReceipts);
        assertFalse(foundReceipts.isEmpty());
        assertEquals(testReceipts.size(), foundReceipts.size());
        assertEquals(testReceipts.get(0).getStoreName(), foundReceipts.get(0).storeName());

        verify(repository).findByPurchaseDate(date);
    }

    @Test
    void findByPurchaseDate_noReceiptExist_shouldReturnEmptyList() {
        final LocalDate date = LocalDate.now();
        when(repository.findByPurchaseDate(date)).thenReturn(Collections.emptyList());
        final List<ReceiptDto> foundReceipts = service.findByPurchaseDate(date);

        assertNotNull(foundReceipts);
        assertTrue(foundReceipts.isEmpty());

        verify(repository).findByPurchaseDate(date);
    }

    @Test
    void findByPurchaseDateBetween_twoReceiptsExist_shouldReturnListOfReceiptDto() {
        final LocalDate dateStart = LocalDate.now();
        final LocalDate dateEnd = dateStart.plusDays(1);
        final Receipt receipt = Receipt.builder()
                .id(1L)
                .storeName("test")
                .purchaseDate(dateStart)
                .build();
        final Receipt receipt2 = Receipt.builder()
                .id(2L)
                .storeName("test")
                .purchaseDate(dateEnd)
                .build();
        final List<Receipt> testReceipts = Arrays.asList(receipt, receipt2);

        when(repository.findByPurchaseDateBetween(dateStart, dateEnd)).thenReturn(testReceipts);

        final List<ReceiptDto> foundReceipts = service.findByPurchaseDateBetween(dateStart, dateEnd);

        assertNotNull(foundReceipts);
        assertFalse(foundReceipts.isEmpty());
        assertEquals(testReceipts.size(), foundReceipts.size());
        assertEquals(testReceipts.get(0).getStoreName(), foundReceipts.get(0).storeName());

        verify(repository).findByPurchaseDateBetween(dateStart, dateEnd);
    }

    @Test
    void findByPurchaseDateBetween_noReceiptExist_shouldReturnEmptyList() {
        final LocalDate dateStart = LocalDate.now();
        final LocalDate dateEnd = dateStart.plusDays(1);
        when(repository.findByPurchaseDateBetween(dateStart, dateEnd)).thenReturn(Collections.emptyList());
        final List<ReceiptDto> foundReceipts = service.findByPurchaseDateBetween(dateStart, dateEnd);

        assertNotNull(foundReceipts);
        assertTrue(foundReceipts.isEmpty());

        verify(repository).findByPurchaseDateBetween(dateStart, dateEnd);
    }

    @Test
    void findByPurchaseDateAfter_oneReceiptExist_shouldReturnListOfReceiptDto() {
        final LocalDate date = LocalDate.now();
        final Receipt receipt = Receipt.builder()
                .id(1L)
                .storeName("test")
                .purchaseDate(date.plusDays(1))
                .build();
        final List<Receipt> testReceipts = Collections.singletonList(receipt);

        when(repository.findByPurchaseDateAfter(date)).thenReturn(testReceipts);

        final List<ReceiptDto> foundReceipts = service.findByPurchaseDateAfter(date);

        assertNotNull(foundReceipts);
        assertFalse(foundReceipts.isEmpty());
        assertEquals(testReceipts.size(), foundReceipts.size());
        assertEquals(testReceipts.get(0).getStoreName(), foundReceipts.get(0).storeName());

        verify(repository).findByPurchaseDateAfter(date);
    }

    @Test
    void findByPurchaseDateAfter_noReceiptExist_shouldReturnEmptyList() {
        final LocalDate date = LocalDate.now();
        when(repository.findByPurchaseDateAfter(date)).thenReturn(Collections.emptyList());
        final List<ReceiptDto> foundReceipts = service.findByPurchaseDateAfter(date);

        assertNotNull(foundReceipts);
        assertTrue(foundReceipts.isEmpty());

        verify(repository).findByPurchaseDateAfter(date);
    }

    @Test
    void findByPurchaseDateBefore_oneReceiptExist_shouldReturnListOfReceiptDto() {
        final LocalDate date = LocalDate.now();
        final Receipt receipt = Receipt.builder()
                .id(1L)
                .storeName("test")
                .purchaseDate(date.minusDays(1))
                .build();
        final List<Receipt> testReceipts = Collections.singletonList(receipt);

        when(repository.findByPurchaseDateBefore(date)).thenReturn(testReceipts);

        final List<ReceiptDto> foundReceipts = service.findByPurchaseDateBefore(date);

        assertNotNull(foundReceipts);
        assertFalse(foundReceipts.isEmpty());
        assertEquals(testReceipts.size(), foundReceipts.size());
        assertEquals(testReceipts.get(0).getStoreName(), foundReceipts.get(0).storeName());

        verify(repository).findByPurchaseDateBefore(date);
    }

    @Test
    void findByPurchaseDateBefore_noReceiptExist_shouldReturnEmptyList() {
        final LocalDate date = LocalDate.now();
        when(repository.findByPurchaseDateBefore(date)).thenReturn(Collections.emptyList());
        final List<ReceiptDto> foundReceipts = service.findByPurchaseDateBefore(date);

        assertNotNull(foundReceipts);
        assertTrue(foundReceipts.isEmpty());

        verify(repository).findByPurchaseDateBefore(date);
    }

    @Test
    void addReceipt_nullReceipt_shouldReturnNull() {
        final WritableReceiptDto receiptDto = null;

        final List<ReceiptDto> response = service.addReceipt(listOf(receiptDto));

        assertNotNull(response);
        assertTrue(response.isEmpty());

        verify(repository, times(0)).save(mapper.writableDtoToEntity(receiptDto));
    }

    @Test
    void addReceipt_correctReceipt_shouldReturnCorrectDto() {
        final LocalDate date = LocalDate.now();
        final ProductDto productDto = new ProductDto("test", BigDecimal.ONE, Measurement.QUANTITY, BigDecimal.ONE, "test");
        final WritableReceiptDto receiptDto = new WritableReceiptDto("test", date, Collections.singletonList(productDto));

        when(repository.save(any())).thenReturn(mapper.writableDtoToEntity(receiptDto));

        final List<ReceiptDto> response = service.addReceipt(listOf(receiptDto));

        assertNotNull(response);
        assertFalse(response.isEmpty());
        assertEquals(receiptDto.storeName(), response.get(0).storeName());
        assertEquals(receiptDto.products().size(), response.get(0).products().size());
        assertEquals(receiptDto.products().get(0), response.get(0).products().get(0));

        verify(repository).save(any());
    }
}