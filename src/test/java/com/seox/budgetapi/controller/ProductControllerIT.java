package com.seox.budgetapi.controller;

import com.seox.budgetapi.dto.ProductDto;
import com.seox.budgetapi.model.enums.Measurement;
import com.seox.budgetapi.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
@ActiveProfiles("test")
@WithMockUser
class ProductControllerIT {
    @MockBean
    ProductService service;
    @Autowired
    MockMvc mockMvc;

    @Test
    void getAllProducts_apiV1_shouldReturnSingleElementList() throws Exception {
        when(service.findAll()).thenReturn(Collections.singletonList(new ProductDto("test", BigDecimal.ONE, Measurement.QUANTITY, BigDecimal.ONE,  "test")));
        mockMvc.perform(get("/api/v1/product/")).andExpect(status().isOk()).andExpect(jsonPath("$[0].name").value("test"));

        verify(service).findAll();
    }

    @Test
    void getAllProducts_incorrectApiVersion_shouldReturnEmptyList() throws Exception {
        mockMvc.perform(get("/api/v/product/")).andExpect(status().isOk()).andExpect(jsonPath("$[0]").doesNotExist());

        verify(service, never()).findAll();
    }

    @Test
    void getProductsByReceiptId_incorrectApiVersion_shouldReturnEmptyList() throws Exception {
        mockMvc.perform(get("/api/v/product/receipt?id=1")).andExpect(status().isOk()).andExpect(jsonPath("$[0]").doesNotExist());

        verify(service, never()).findByReceiptId(1L);
    }

    @Test
    void getProductsByReceiptId_apiV1_shouldReturnSingleElementList() throws Exception {
        when(service.findByReceiptId(1L)).thenReturn(Collections.singletonList(new ProductDto("test", BigDecimal.ONE, Measurement.QUANTITY, BigDecimal.ONE, "test")));
        mockMvc.perform(get("/api/v1/product/receipt?id=1")).andExpect(status().isOk()).andExpect(jsonPath("$[0].name").value("test"));

        verify(service).findByReceiptId(1L);
    }

    @Test
    void testGetProductsByName_incorrectApiVersion_shouldReturnEmptyList() throws Exception {
        mockMvc.perform(get("/api/v/product?productName=test")).andExpect(status().isOk()).andExpect(jsonPath("$[0]").doesNotExist());

        verify(service, never()).findByName("test");
    }

    @Test
    void testGetProductsByName_apiV1_shouldReturnSingleElementList() throws Exception {
        when(service.findByName("test")).thenReturn(Collections.singletonList(new ProductDto("test", BigDecimal.ONE, Measurement.QUANTITY, BigDecimal.ONE, "test")));
        mockMvc.perform(get("/api/v1/product?productName=test")).andExpect(status().isOk()).andExpect(jsonPath("$[0].name").value("test"));

        verify(service).findByName("test");
    }
}