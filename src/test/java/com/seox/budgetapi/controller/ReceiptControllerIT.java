package com.seox.budgetapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.seox.budgetapi.dto.ProductDto;
import com.seox.budgetapi.dto.ReceiptDto;
import com.seox.budgetapi.dto.WritableReceiptDto;
import com.seox.budgetapi.model.enums.Measurement;
import com.seox.budgetapi.service.ReceiptService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;

@WebMvcTest(ReceiptController.class)
@ActiveProfiles("test")
@WithMockUser
class ReceiptControllerIT {

    @MockBean
    ReceiptService service;
    @Autowired
    MockMvc mockMvc;

    @Test
    void getAllReceipts_apiV1_shouldReturnSingleElementList() throws Exception {
        when(service.findAll()).thenReturn(Collections.singletonList(new ReceiptDto("test", LocalDate.now(), null)));
        mockMvc.perform(get("/api/v1/receipt/")).andExpect(status().isOk()).andExpect(jsonPath("$[0].storeName").value("test"));

        verify(service).findAll();
    }

    @Test
    void getAllReceipts_incorrectApiVersion_shouldReturnEmptyList() throws Exception {
        mockMvc.perform(get("/api/v/receipt/")).andExpect(status().isOk()).andExpect(jsonPath("$[0]").doesNotExist());

        verify(service, never()).findAll();
    }

    @Test
    void getProductsByStoreName_incorrectApiVersion_shouldReturnEmptyList() throws Exception {
        mockMvc.perform(get("/api/v/receipt/store?storeName=test")).andExpect(status().isOk()).andExpect(jsonPath("$[0]").doesNotExist());

        verify(service, never()).findByStoreName("test");
    }

    @Test
    void getProductsByStoreName_apiV1_shouldReturnSingleElementList() throws Exception {
        when(service.findByStoreName("test")).thenReturn(Collections.singletonList(new ReceiptDto("test", LocalDate.now(), null)));
        mockMvc.perform(get("/api/v1/receipt/store?storeName=test")).andExpect(status().isOk()).andExpect(jsonPath("$[0].storeName").value("test"));

        verify(service).findByStoreName("test");
    }

    @Test
    void testGetProductsByPurchaseDate_incorrectApiVersion_shouldReturnEmptyList() throws Exception {
        mockMvc.perform(get("/api/v/receipt?purchaseDate=2021-01-01")).andExpect(status().isOk()).andExpect(jsonPath("$[0]").doesNotExist());

        verify(service, never()).findByPurchaseDate(LocalDate.of(2021, 1, 1));
    }

    @Test
    void testGetProductsByPurchaseDate_apiV1_shouldReturnSingleElementList() throws Exception {
        when(service.findByPurchaseDate(LocalDate.of(2021, 1, 1))).thenReturn(Collections.singletonList(new ReceiptDto("test", LocalDate.now(), null)));
        mockMvc.perform(get("/api/v1/receipt?purchaseDate=2021-01-01")).andExpect(status().isOk()).andExpect(jsonPath("$[0].storeName").value("test"));

        verify(service).findByPurchaseDate(LocalDate.of(2021, 1, 1));
    }

    @Test
    void testGetProductsByPurchaseDateBetween_incorrectApiVersion_shouldReturnEmptyList() throws Exception {
        mockMvc.perform(get("/api/v/receipt/between?purchaseDateStart=2021-01-01&purchaseDateEnd=2022-01-01")).andExpect(status().isOk()).andExpect(jsonPath("$[0]").doesNotExist());

        verify(service, never()).findByPurchaseDateBetween(LocalDate.of(2021, 1, 1), LocalDate.of(2022, 1, 1));
    }

    @Test
    void testGetProductsByPurchaseDateBetween_apiV1_shouldReturnSingleElementList() throws Exception {
        when(service.findByPurchaseDateBetween(LocalDate.of(2021, 1, 1), LocalDate.of(2022, 1, 1))).thenReturn(Collections.singletonList(new ReceiptDto("test", LocalDate.now(), null)));
        mockMvc.perform(get("/api/v1/receipt/between?purchaseDateStart=2021-01-01&purchaseDateEnd=2022-01-01")).andExpect(status().isOk()).andExpect(jsonPath("$[0].storeName").value("test"));

        verify(service).findByPurchaseDateBetween(LocalDate.of(2021, 1, 1), LocalDate.of(2022, 1, 1));
    }

    @Test
    void testGetProductsByPurchaseDateAfter_incorrectApiVersion_shouldReturnEmptyList() throws Exception {
        mockMvc.perform(get("/api/v/receipt/after?purchaseDateAfter=2021-01-01")).andExpect(status().isOk()).andExpect(jsonPath("$[0]").doesNotExist());

        verify(service, never()).findByPurchaseDateAfter(LocalDate.of(2021, 1, 1));
    }

    @Test
    void testGetProductsByPurchaseDateAfter_apiV1_shouldReturnSingleElementList() throws Exception {
        when(service.findByPurchaseDateAfter(LocalDate.of(2021, 1, 1))).thenReturn(Collections.singletonList(new ReceiptDto("test", LocalDate.now(), null)));
        mockMvc.perform(get("/api/v1/receipt/after?purchaseDateAfter=2021-01-01")).andExpect(status().isOk()).andExpect(jsonPath("$[0].storeName").value("test"));

        verify(service).findByPurchaseDateAfter(LocalDate.of(2021, 1, 1));
    }

    @Test
    void testGetProductsByPurchaseDateBefore_incorrectApiVersion_shouldReturnEmptyList() throws Exception {
        mockMvc.perform(get("/api/v/receipt/before?purchaseDateBefore=2021-01-01")).andExpect(status().isOk()).andExpect(jsonPath("$[0]").doesNotExist());

        verify(service, never()).findByPurchaseDateBefore(LocalDate.of(2021, 1, 1));
    }

    @Test
    void testGetProductsByPurchaseDateBefore_apiV1_shouldReturnSingleElementList() throws Exception {
        when(service.findByPurchaseDateBefore(LocalDate.of(2021, 1, 1))).thenReturn(Collections.singletonList(new ReceiptDto("test", LocalDate.now(), null)));
        mockMvc.perform(get("/api/v1/receipt/before?purchaseDateBefore=2021-01-01")).andExpect(status().isOk()).andExpect(jsonPath("$[0].storeName").value("test"));

        verify(service).findByPurchaseDateBefore(LocalDate.of(2021, 1, 1));
    }

    @Test
    void testAddReceipt_incorrectApiVersion_shouldReturnEmptyList() throws Exception {
        final List<ProductDto> productList = Collections.singletonList(new ProductDto("test", BigDecimal.ONE, Measurement.QUANTITY, BigDecimal.ONE, "test"));
        final List<WritableReceiptDto> receiptList = Collections.singletonList(new WritableReceiptDto("test", LocalDate.now(), productList));
        mockMvc.perform(post("/api/v/receipt/add").with(csrf()).contentType(MediaType.APPLICATION_JSON).content(encodeDtoAsJson(receiptList))).andExpect(status().isOk()).andExpect(jsonPath("$[0]").doesNotExist());

        verify(service, never()).addReceipt(receiptList);
    }

    @Test
    void testAddReceipt_apiV1_shouldReturnSingleElementList() throws Exception {
        final List<ProductDto> productList = Collections.singletonList(new ProductDto("test", BigDecimal.ONE, Measurement.QUANTITY, BigDecimal.ONE, "test"));
        final List<WritableReceiptDto> receiptList = Collections.singletonList(new WritableReceiptDto("test", LocalDate.now(), productList));
        when(service.addReceipt(receiptList)).thenReturn(Collections.singletonList(new ReceiptDto(receiptList.get(0).storeName(), receiptList.get(0).purchaseDate(), receiptList.get(0).products())));

        mockMvc.perform(post("/api/v1/receipt/add").with(csrf()).contentType(MediaType.APPLICATION_JSON).content(encodeDtoAsJson(receiptList))).andExpect(status().isOk()).andExpect(jsonPath("status").value("Success"));

        verify(service).addReceipt(receiptList);
    }

    @SneakyThrows
    String encodeDtoAsJson(final List<WritableReceiptDto> receiptDto) {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper.writeValueAsString(receiptDto);
    }
}