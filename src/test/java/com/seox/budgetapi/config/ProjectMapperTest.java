package com.seox.budgetapi.config;

import com.seox.budgetapi.dto.ProductDto;
import com.seox.budgetapi.dto.ReceiptDto;
import com.seox.budgetapi.dto.WritableReceiptDto;
import com.seox.budgetapi.model.Product;
import com.seox.budgetapi.model.Receipt;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = ProjectMapperImpl.class)
class ProjectMapperTest {

    @Autowired
    ProjectMapper projectMapper;

    @Test
    void mapEntityToDto_withName_shouldReturnSameNameInDto() {
        Product product = new Product();
        product.setName("1");
        ProductDto dto = projectMapper.entityToDto(product);

        assertEquals(product.getName(), dto.name());
    }

    @Test
    void mapEntityToDto_withNulls_shouldReturnDtoWithNulls() {
        Product product = new Product();
        ProductDto productDto = projectMapper.entityToDto(product);

        Receipt receipt = new Receipt();
        ReceiptDto receiptDto = projectMapper.entityToDto(receipt);

        assertNull(productDto.name());
        assertNull(receiptDto.storeName());
    }

    @Test
    void mapEntityToDto_withStoreName_shouldReturnSameNameInDto() {
        Receipt receipt = new Receipt();
        receipt.setStoreName("1");
        receipt.setProducts(Collections.singletonList(Product.builder().name("test").build()));
        ReceiptDto dto = projectMapper.entityToDto(receipt);

        assertEquals(receipt.getStoreName(), dto.storeName());
        assertEquals(receipt.getProducts().size(), dto.products().size());
        assertEquals(receipt.getProducts().get(0).getName(), dto.products().get(0).name());
    }

    @Test
    void mapDtoToEntity_withName_shouldReturnSameInEntity() {
        ProductDto dto = new ProductDto("1", null, null, null, null);
        Product product = projectMapper.dtoToEntity(dto);

        assertEquals(dto.name(), product.getName());
    }

    @Test
    void mapDtoToEntity_withStoreName_shouldReturnSameInEntity() {
        ReceiptDto dto = new ReceiptDto("1", null, Collections.singletonList(new ProductDto("test", null, null, null, null)));
        Receipt receipt = projectMapper.dtoToEntity(dto);

        assertEquals(dto.storeName(), receipt.getStoreName());
        assertEquals(dto.products().size(), receipt.getProducts().size());
    }

    @Test
    void mapDtoToEntity_withNulls_shouldReturnEntityWithNulls() {
        ProductDto productDto = new ProductDto(null, null, null, null, null);
        Product product = projectMapper.dtoToEntity(productDto);

        ReceiptDto receiptDto = new ReceiptDto(null, null, null);
        Receipt receipt = projectMapper.dtoToEntity(receiptDto);

        assertNull(product.getName());
        assertNull(receipt.getStoreName());
    }

    @Test
    void mapEntityToDto_withNullEntity_shouldReturnNull() {
        Product product = null;
        ProductDto productDto = projectMapper.entityToDto(product);

        Receipt receipt = null;
        ReceiptDto receiptDto = projectMapper.entityToDto(receipt);

        assertNull(productDto);
        assertNull(receiptDto);
    }

    @Test
    void mapWritableDtoToEntity_withStoreName_shouldReturnSameInEntity() {
        ReceiptDto dto = new ReceiptDto("1", null, Collections.singletonList(new ProductDto("test", null, null, null, null)));
        Receipt receipt = projectMapper.dtoToEntity(dto);

        assertEquals(dto.storeName(), receipt.getStoreName());
        assertEquals(dto.products().size(), receipt.getProducts().size());
    }

    @Test
    void mapWritableDtoToEntity_withNulls_shouldReturnEntityWithNulls() {
        WritableReceiptDto receiptDto = new WritableReceiptDto(null, null, null);
        Receipt receipt = projectMapper.writableDtoToEntity(receiptDto);

        assertNull(receipt.getStoreName());
    }

    @Test
    void mapWritableDtoToEntity_withNullDto_shouldReturnNull() {
        WritableReceiptDto receiptDto = null;
        Receipt receipt = projectMapper.writableDtoToEntity(receiptDto);

        assertNull(receipt);
    }

    @Test
    void mapDtoToEntity_withNullDto_shouldReturnNull() {
        ProductDto productDto = null;
        Product product = projectMapper.dtoToEntity(productDto);

        ReceiptDto receiptDto = null;
        Receipt receipt = projectMapper.dtoToEntity(receiptDto);

        assertNull(product);
        assertNull(receipt);
    }
}